package com.pkpservice.shippingservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ShipperResponse implements Serializable {
    private Long id;
    private String code;
    private String name;
}
