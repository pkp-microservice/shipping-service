package com.pkpservice.shippingservice.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponse implements Serializable {
    private String id;
    private String transactionId;
    private String customerId;
    private Long amount;
}
