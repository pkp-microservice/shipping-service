package com.pkpservice.shippingservice.model.request;

import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductRequest {
    @NotNull
    private String id;
    @NotNull
    private String code;
    @NotNull
    private String name;
    @NotNull
    private Long price;
    private Long ttl;
}
