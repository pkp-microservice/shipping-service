package com.pkpservice.shippingservice.model.redis;

import com.pkpservice.shippingservice.model.request.ProductRequest;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.keyvalue.annotation.KeySpace;
import org.springframework.data.redis.core.TimeToLive;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@KeySpace("product")
public class ProductEntity implements Serializable {
    @Id
    private String id;
    private String code;
    private String name;
    private Long price;
    @TimeToLive
    private Long ttl = -1L;

    public ProductEntity(ProductRequest request) {
        BeanUtils.copyProperties(request, this);
    }
}
