package com.pkpservice.shippingservice.repository;

import com.pkpservice.shippingservice.model.redis.ProductEntity;
import org.springframework.data.keyvalue.repository.KeyValueRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends KeyValueRepository<ProductEntity, String> {
}
