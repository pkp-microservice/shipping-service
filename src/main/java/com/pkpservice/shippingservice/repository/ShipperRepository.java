package com.pkpservice.shippingservice.repository;

import com.pkpservice.shippingservice.model.entity.ShipperEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ShipperRepository extends JpaRepository<ShipperEntity, Long> {
}
