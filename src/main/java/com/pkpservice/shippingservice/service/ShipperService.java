package com.pkpservice.shippingservice.service;

import com.pkpservice.shippingservice.model.request.PageFilterRequest;
import com.pkpservice.shippingservice.model.response.ShipperResponse;
import org.springframework.data.domain.Page;

import java.util.List;

public interface ShipperService {
    Page<ShipperResponse> getPage(PageFilterRequest filter);
    List<ShipperResponse> getAll();
}
