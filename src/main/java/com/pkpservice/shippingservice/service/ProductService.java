package com.pkpservice.shippingservice.service;

import com.pkpservice.shippingservice.model.redis.ProductEntity;
import com.pkpservice.shippingservice.model.request.ProductRequest;

public interface ProductService {
    String save(ProductRequest entity);
    Object getById(String id);
}
