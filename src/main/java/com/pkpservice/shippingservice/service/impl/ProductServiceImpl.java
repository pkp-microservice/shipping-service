package com.pkpservice.shippingservice.service.impl;

import com.pkpservice.shippingservice.exception.CommonApiException;
import com.pkpservice.shippingservice.model.redis.ProductEntity;
import com.pkpservice.shippingservice.model.request.ProductRequest;
import com.pkpservice.shippingservice.repository.ProductRepository;
import com.pkpservice.shippingservice.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {
    private ProductRepository productRepository;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository) {
        this.productRepository = productRepository;
    }

    @Override
    public String save(ProductRequest request) {
        ProductEntity entity = new ProductEntity(request);
        try{
            productRepository.save(entity);
            return "SUCCESS";
        }catch (RedisSystemException e){
            throw new CommonApiException(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Override
    @Cacheable(value = "product", key = "#id")
    public Object getById(String id) {
        return null;
    }
}
