package com.pkpservice.shippingservice.service.impl;

import com.pkpservice.shippingservice.model.request.PageFilterRequest;
import com.pkpservice.shippingservice.model.response.ShipperResponse;
import com.pkpservice.shippingservice.service.ShipperService;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ShipperServiceImpl implements ShipperService {
    @Override
    public Page<ShipperResponse> getPage(PageFilterRequest filter) {
        return null;
    }

    @Override
    public List<ShipperResponse> getAll() {
        return null;
    }
}
