package com.pkpservice.shippingservice.config;

import com.pkpservice.shippingservice.model.response.OrderResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.stream.StreamListener;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class OrderListener implements StreamListener<String, ObjectRecord<String, OrderResponse>> {
    @Override
    public void onMessage(ObjectRecord<String, OrderResponse> message) {
        log.info("Receive message, {}", message.getValue());
    }
}
