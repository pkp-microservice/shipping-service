package com.pkpservice.shippingservice.config;

import com.pkpservice.shippingservice.model.response.OrderResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class OrderPublisher {
    private StringRedisTemplate redisTemplate;

    @Autowired
    public OrderPublisher(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Scheduled(fixedRate = 10, timeUnit = TimeUnit.SECONDS)
    public void publish(){
        OrderResponse order = new OrderResponse(UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                UUID.randomUUID().toString(),
                10_0000L);
        ObjectRecord<String, OrderResponse> record = ObjectRecord.create("orders", order);
        redisTemplate.opsForStream().add(record);
    }
}
