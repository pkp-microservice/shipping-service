package com.pkpservice.shippingservice.config;

import com.pkpservice.shippingservice.model.request.ShipperRequest;
import com.pkpservice.shippingservice.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Component
public class CustomerPublisher {
    private StringRedisTemplate redisTemplate;
    private CommonUtil commonUtil;

    @Autowired
    public CustomerPublisher(StringRedisTemplate redisTemplate, CommonUtil commonUtil) {
        this.redisTemplate = redisTemplate;
        this.commonUtil = commonUtil;
    }

    @Scheduled(fixedRate = 3, timeUnit = TimeUnit.SECONDS)
    public void publish(){
        ShipperRequest request = new ShipperRequest(1L, commonUtil.getAlphaNumericString(5), commonUtil.getAlphaNumericString(20));
        //redisTemplate.convertAndSend("customers", "CustomerID : "+ commonUtil.getAlphaNumericString(5) + " TrxID : "+ commonUtil.getAlphaNumericString(10));
        redisTemplate.convertAndSend("customers", request.toString());
    }
}
