package com.pkpservice.shippingservice.config;

import com.pkpservice.shippingservice.model.response.OrderResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.stream.Consumer;
import org.springframework.data.redis.connection.stream.ObjectRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.listener.ChannelTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.stream.StreamMessageListenerContainer;
import org.springframework.data.redis.stream.Subscription;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.time.Duration;

@Slf4j
@Configuration
@EnableScheduling
public class RedisConfig {
    private StringRedisTemplate redisTemplate;

    @Autowired
    public RedisConfig(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Bean(destroyMethod = "stop", initMethod = "start")
    public StreamMessageListenerContainer<String, ObjectRecord<String, OrderResponse>> orderContainer(RedisConnectionFactory connectionFactory){
        var options = StreamMessageListenerContainer.StreamMessageListenerContainerOptions
                .builder()
                .pollTimeout(Duration.ofSeconds(5))
                .targetType(OrderResponse.class)
                .build();
        return StreamMessageListenerContainer.create(connectionFactory, options);
    }

    @Bean
    public Subscription orderSubscription(StreamMessageListenerContainer<String, ObjectRecord<String, OrderResponse>> orderContainer,
                                          OrderListener orderListener){
        try{
            redisTemplate.opsForStream().createGroup("orders","order-group");
        }catch (Throwable e){
            log.info("Group sudah ada");
        }

        var offset = StreamOffset.create("orders", ReadOffset.lastConsumed());
        var consumer = Consumer.from("order-group","consumer-1");
        var readRequest = StreamMessageListenerContainer.StreamReadRequest
                .builder(offset)
                .consumer(consumer)
                .autoAcknowledge(true)
                .cancelOnError(throwable -> false)
                .errorHandler(throwable -> log.warn(throwable.getMessage()))
                .build();

        return orderContainer.register(readRequest, orderListener);
    }

    @Bean
    public RedisMessageListenerContainer messageListenerContainer(RedisConnectionFactory factory,
                                                                  CustomerListener customerListener){
        var container = new RedisMessageListenerContainer();
        container.setConnectionFactory(factory);
        container.addMessageListener(customerListener, new ChannelTopic("customers"));
        return container;
    }
}
