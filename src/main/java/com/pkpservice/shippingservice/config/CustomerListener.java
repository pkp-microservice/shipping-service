package com.pkpservice.shippingservice.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.pkpservice.shippingservice.model.request.ShipperRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.data.redis.serializer.SerializationUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Slf4j
@Component
public class CustomerListener implements MessageListener {
    @Override
    public void onMessage(Message message, byte[] pattern) {
        log.info("Receive customer message: {}", new String(message.getBody()));
    }
}
