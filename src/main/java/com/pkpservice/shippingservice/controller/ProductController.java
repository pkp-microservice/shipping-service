package com.pkpservice.shippingservice.controller;

import com.pkpservice.shippingservice.model.request.ProductRequest;
import com.pkpservice.shippingservice.model.response.Response;
import com.pkpservice.shippingservice.service.ProductService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/product")
public class ProductController {
    private ProductService service;

    @Autowired
    public ProductController(ProductService service) {
        this.service = service;
    }

    @PostMapping
    private ResponseEntity<Response> save(@RequestBody @Valid ProductRequest request){
        var result = service.save(request);
        return ResponseEntity.ok()
                .body( Response.builder()
                        .statusCode(200)
                        .data(result)
                        .message("SUCCESS")
                        .build()
                );
    }
}
