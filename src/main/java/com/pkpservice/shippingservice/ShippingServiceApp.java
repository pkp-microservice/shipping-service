package com.pkpservice.shippingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;

@SpringBootApplication
@EnableRedisRepositories
@EnableCaching
public class ShippingServiceApp {

    public static void main(String[] args) {
        SpringApplication.run(ShippingServiceApp.class, args);
    }

}
