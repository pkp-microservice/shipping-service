package com.pkpservice.shippingservice;


import com.pkpservice.shippingservice.model.request.PageFilterRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataAccessException;
import org.springframework.data.geo.Distance;
import org.springframework.data.geo.Metrics;
import org.springframework.data.geo.Point;
import org.springframework.data.redis.RedisSystemException;
import org.springframework.data.redis.connection.stream.Consumer;
import org.springframework.data.redis.connection.stream.MapRecord;
import org.springframework.data.redis.connection.stream.ReadOffset;
import org.springframework.data.redis.connection.stream.StreamOffset;
import org.springframework.data.redis.core.*;
import org.springframework.data.redis.support.collections.RedisList;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
@SpringBootTest
class RedisServiceTest {
    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    void redisTemplateTest() {
        Assertions.assertNotNull(redisTemplate);
    }

    @Test
    void stringTest() throws InterruptedException {
        ValueOperations<String, String> operations = redisTemplate.opsForValue();
        operations.set("datas","roni");
        assertEquals("roni", operations.get("datas"));

        operations.set("datas","name-test", Duration.ofSeconds(4));
        Thread.sleep(Duration.ofSeconds(3).toMillis());
        Assertions.assertNotNull(operations.get("datas"));

        operations.set("datas","name-test", Duration.ofSeconds(2));
        Thread.sleep(Duration.ofSeconds(3).toMillis());
        Assertions.assertNull(operations.get("datas"));
    }

    @Test
    void listTest() {
        ListOperations<String, String> operations = redisTemplate.opsForList();
        operations.rightPush("names","ahmad");
        operations.rightPush("names","Dedy");
        operations.rightPush("names","Amin");
        operations.rightPush("names","Sukron");

        assertEquals("Sukron",operations.rightPop("names"));
        assertEquals("ahmad",operations.leftPop("names"));
        assertEquals("Amin",operations.rightPop("names"));
        assertEquals("Dedy",operations.rightPop("names"));
    }

    @Test
    void setTest() {
        SetOperations<String, String> operations = redisTemplate.opsForSet();
        operations.add("students","roni");
        operations.add("students","roni");
        operations.add("students","roni");
        operations.add("students","Deddy");
        operations.add("students","Deddy");
        operations.add("students","Deddy");
        operations.add("students","Arif");
        operations.add("students","Arif");

        assertEquals(3, operations.members("students").size());
        assertThat(operations.members("students"), hasItems("roni","Deddy","Arif"));
    }

    @Test
    void zSetTest() {
        ZSetOperations<String, String> operations = redisTemplate.opsForZSet();
        operations.add("scores","Ahmad",100);
        operations.add("scores","Zainudin",90);
        operations.add("scores","Rudi",85);
        operations.add("scores","Doni",95);

        assertEquals("Ahmad", operations.popMax("scores").getValue());
        assertEquals("Doni", operations.popMax("scores").getValue());
        assertEquals("Rudi", operations.popMin("scores").getValue());
        assertEquals("Zainudin", operations.popMax("scores").getValue());
    }

    @Test
    void hashTest() {
        HashOperations<String, Object, Object> operations = redisTemplate.opsForHash();
        operations.put("user:1","id","1");
        operations.put("user:1","name","ahmad");
        operations.put("user:1","email","ahamd@gmail.com");
        operations.put("user:1","phone","081323232");

        assertEquals("1",operations.get("user:1","id"));
        assertEquals("ahmad",operations.get("user:1","name"));
        assertEquals("ahamd@gmail.com",operations.get("user:1","email"));
        assertEquals("081323232",operations.get("user:1","phone"));
    }

    @Test
    void geoTest() {
        GeoOperations<String, String> operations = redisTemplate.opsForGeo();
        operations.add("seller", new Point(106.940449,-6.387523),"Toko 1");
        operations.add("seller", new Point(106.894853,-6.371017),"Toko 2");

        Distance distance = operations.distance("seller", "Toko 1", "Toko 2", Metrics.KILOMETERS);
        System.out.println("distance : "+ distance.getValue());
        assertEquals(5.3642, distance.getValue());
    }

    @Test
    void hiperlogLog() {
        HyperLogLogOperations<String, String> operations = redisTemplate.opsForHyperLogLog();
        operations.add("matrics","Roni", "Ahmad", "Sobari");
        operations.add("matrics","Sobari", "Ahmad", "Roni");
        operations.add("matrics","Syakuri", "Syukur", "Sobari");
        operations.add("matrics","Syukur", "Siti", "Akmal");

        System.out.println("size: "+ operations.size("matrics"));
        assertEquals(7, operations.size("matrics"));
    }

    @Test
    void transactionTest() {
        redisTemplate.execute(new SessionCallback<>() {
            @Override
            public Object execute(RedisOperations operations) throws DataAccessException {
                redisTemplate.multi();
                redisTemplate.opsForValue().set("test1","test1-value");
                redisTemplate.opsForValue().set("test2","test2-value");
                redisTemplate.opsForValue().set("test3","test3-value");
                redisTemplate.exec();
                return null;
            }
        });

        assertEquals("test1-value", redisTemplate.opsForValue().get("test1"));
        assertEquals("test2-value", redisTemplate.opsForValue().get("test2"));
        assertEquals("test3-value", redisTemplate.opsForValue().get("test3"));
    }

    @Test
    void introductionListTest() {
        List<PageFilterRequest> pages = new ArrayList<>();
        pages.add(new PageFilterRequest(10,0,""));
        pages.add(new PageFilterRequest(10,1,""));
        pages.add(new PageFilterRequest(10,2,""));

        assertEquals(3, pages.size());
        for(PageFilterRequest item: pages){
            System.out.println("pageSize: "+ item.getPageSize());
            System.out.println("keySearch: "+ item.getKeySearch());
        }

        for (int i = 0; i < pages.size(); i++) {
            PageFilterRequest item = pages.get(0);
        }

        pages.forEach(item -> {

        });
    }

    @Test
    void pipelineTest() {
        List<Object> data = redisTemplate.executePipelined(new SessionCallback<Object>() {
            @Override
            public Object execute(RedisOperations operations) throws DataAccessException {
                redisTemplate.opsForValue().set("test1","test1-value");
                redisTemplate.opsForValue().set("test2","test2-value");
                redisTemplate.opsForValue().set("test3","test3-value");
                redisTemplate.opsForValue().set("test4","test4-value");
                redisTemplate.opsForValue().set("test5","test5-value");
                return null;
            }
        });

        assertThat(data, hasSize(5));
        assertThat(data, hasItems(true));
    }

    @Test
    void publishTest() {
        StreamOperations<String, Object, Object> operations = redisTemplate.opsForStream();
        var record = MapRecord.create("stream-1", Map.of(
                "name", "Ahamad",
                "address", "Ciamis",
                "gender", "Male",
                "email", "ahmad@gmail.com"
        ));

        for (int i = 0; i < 10; i++) {
            operations.add(record);
        }
    }

    @Test
    void subscribeTest() {
        var operations = redisTemplate.opsForStream();
        try{
            operations.createGroup("stream-1","sample-group");
        }catch (RedisSystemException e){
            log.error("Redis error; {}", e.getMessage());
        }

        Consumer from = Consumer.from("sample-group", "stream-1");
        StreamOffset<String> stringStreamOffset = StreamOffset.create("stream-1", ReadOffset.lastConsumed());
        List<MapRecord<String, Object, Object>> records = operations.read(from, stringStreamOffset);

        for (var record: records){
            System.out.println("record"+ record);
        }
    }

    @Test
    void redisListTest() {
        List<String> names = RedisList.create("names-list", redisTemplate);
        names.add("name-1");
        names.add("name-2");
        names.add("name-3");
        names.add("name-4");

        List<String> result = redisTemplate.opsForList().range("names-list", 0, -1);
        assertThat(names, hasItems("name-1","name-2","name-3", "name-4"));
        assertThat(result, hasItems("name-1","name-2","name-3", "name-4"));
    }
}
